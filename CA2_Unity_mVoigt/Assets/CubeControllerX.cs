﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeControllerX : MonoBehaviour
{
    //Creating editable float variables via editor: maximum & minimum Range the cube can be moved in the x-Axis
    [SerializeField] float minX;
    [SerializeField] float maxX;
    //Creating editable float variable via editor: Determines the speed the cube moves from within the x-Axis
    [SerializeField] float speed;
    //Instantiating Counter Class
    Counter CountX = new Counter();



    void Update()
    {
        //Constantly moving the object by 1 in relation to speed and Time. As the speed variable can change the movement speed, the x-value is used as a constant.
        gameObject.transform.position += new Vector3(1, 0, 0) * speed * Time.deltaTime;

        //Using Maximum() & Minimum() Method to check if cube is within certain range.
        Maximum();
        Minimum();    
    }

    void Maximum()
    {
        //If the gameobject's position of x is larger then the variable maxX, this method will be executed.
        if (gameObject.transform.position.x > maxX)
        {
            //Inverting Speed
            speed = speed * -1;
        }
    }

    void Minimum()
    {
        //If the gameobject's position of x is lesser then the variable minX, this method will be executed.
        if (gameObject.transform.position.x < minX)
        {
            //Inverting Speed
           speed = speed * -1;
        }
    }
    //Method to be executed, when Input of a mouseclick on object is detected.
    void OnMouseDown()
    {
        //Executing method CountHit of the Counter class
        CountX.CountHit();
        //Output Console Message
        Debug.Log("Clicks on CubeY: " + CountX.hitCounter);
    }
}
