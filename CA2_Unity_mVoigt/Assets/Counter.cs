﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Counter 
{
    //Creating variable which counts hits on objects. Making it public to be accesible.
    public int hitCounter = 0;
    //Creating variable used to reset hitCounter variable to a certain value. 
    public int reset = 0;
    //Creating variable used to determine the maximum amount of possible clicks on an object before reset.
    public int maxClick = 10;

    public void CountHit()
    {
        //Increasing variable hitcounter by 1
        hitCounter++;

        //Checking if variable hitCounter is larger than variable maxClick. If so, method will be executed.
        if (hitCounter > maxClick)
        {
            //Setting variable hitCounter to variable reset.
            hitCounter = reset;
            //Console Output
            Debug.Log("Count is resetted to " + reset +" after " + maxClick + " Clicks.");
        }
    }

}
