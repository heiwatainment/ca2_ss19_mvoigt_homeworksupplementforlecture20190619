﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CubeControllerY : MonoBehaviour
{
    //Creating editable float variables via editor: maximum & minimum Range the cube can be moved in the y-Axis
    [SerializeField] float minY;
    [SerializeField] float maxY;
    //Creating editable float variable via editor: Determines the speed the cube moves from within the y-Axis
    [SerializeField] float speed;
    //Instantiating Counter Class
    Counter CountY = new Counter();


    void Update()
    {
        //Constantly moving the object by 1 in relation to speed and Time. As the speed variable can change the movement speed, the y-value is used as a constant.
        gameObject.transform.position += new Vector3(0, 1, 0) * speed * Time.deltaTime;

        //Using Maximum() & Minimum() Method to check if cube is within certain range. Will be checked via Update every frame.
        Maximum();
        Minimum();
    }

    void Maximum()
    {
        //If the gameobject's position of y is larger then the variable maxY, this method will be executed.
        if (gameObject.transform.position.y > maxY)
        {
            //Inverting Speed
            speed = speed * -1;
        }
        
    }


    void Minimum()
    {
        //If the gameobject's position of y is lesser then the variable minY, this method will be executed.
        if (gameObject.transform.position.y < minY)
        {
            //Inverting Speed
            speed = speed * -1;
        }
    }

    //Method to be executed, when Input of a mouseclick on object is detected.
    void OnMouseDown()
    {
        //Executing method CountHit of the Counter class
        CountY.CountHit();
        //Output Console Message
        Debug.Log("Clicks on CubeY: " + CountY.hitCounter);
    }
}
